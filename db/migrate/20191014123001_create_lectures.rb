class CreateLectures < ActiveRecord::Migration
  def change
    create_table :lectures do |t|
      t.string :lectname
      t.string :lecttext 
      t.integer :user_id     
t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :lectures, [:user_id, :created_at]
  end
end
